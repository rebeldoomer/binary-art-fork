import os
import random

# Length of the sequence
length_of_sequence = 1000

# Generate a random sequence of '1's and '0's
binary_sequence = ''.join(random.choice(['0', '1']) for _ in range(length_of_sequence))

# Expand the file path
file_path = os.path.expanduser('~/Documents/binary_input.txt')

# Write the sequence to a file
with open(file_path, 'w') as file:
    file.write(binary_sequence)

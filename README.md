# Binary Art fork

## Description
A program that generates a binary image from a text file containing a sequence of ones and zeros, used to fill an empty black image with green squares (or circles) and gets saved as a .png file. This is a fork of someone else's work.

## Example

![Art Preview](binary_art.png)

## Installation
N/A as of writing

## Authors and acknowledgment
Original project by Domas182 can be found here: https://github.com/Domas182/btopix


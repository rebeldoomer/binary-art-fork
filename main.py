import sys
from PIL import Image, ImageDraw
from tqdm import tqdm
import tkinter as tk
from tkinter import filedialog, simpledialog, messagebox
import random

def read_input_file(file_path):
    try:
        with open(file_path, 'r') as file:
            contents = file.read()
    except IOError:
        messagebox.showerror("Error", "File not found or cannot be opened.")
        sys.exit(1)
    if not set(contents.replace(' ', '')).issubset({'0', '1'}):
        messagebox.showerror("Error", "Input file contains invalid characters.")
        sys.exit(1)
    return contents.replace(' ', '')

def generate_image(data, width_limit=2000, square_size=10, color="green", shape="square"):
    data_length = len(data)
    width = min(data_length * square_size, width_limit)
    height = ((data_length - 1) // (width_limit // square_size) + 1) * square_size

    image = Image.new("RGB", (width, height), "black")
    draw = ImageDraw.Draw(image)

    x, y = 0, 0
    for i in tqdm(range(data_length), desc="Generating image"):
        if data[i] == '1':
            if shape == 'square':
                draw.rectangle([x, y, x + square_size - 1, y + square_size - 1], fill=color)
            elif shape == 'circle':
                radius = square_size // 2
                draw.ellipse([x, y, x + square_size, y + square_size], fill=color)

        x += square_size
        if x >= width_limit:
            x = 0
            y += square_size

    return image

def main():
    root = tk.Tk()
    root.withdraw()  # Hide the main window

    input_file = filedialog.askopenfilename(title="Select Input File")
    if not input_file:
        messagebox.showinfo("No File", "No file selected. Exiting.")
        sys.exit(0)

    output_file = filedialog.asksaveasfilename(defaultextension=".png", filetypes=[("PNG files", "*.png")], title="Save Image As")
    if not output_file:
        messagebox.showinfo("No File", "No save file selected. Exiting.")
        sys.exit(0)

    shape_choice = simpledialog.askstring("Shape", "Enter 'square' or 'circle' for shape:")
    if shape_choice not in ['square', 'circle']:
        messagebox.showinfo("Invalid Shape", "Shape not recognized. Using default 'square'.")
        shape_choice = 'square'
    elif shape_choice is None:  # User pressed cancel.
        messagebox.showinfo("Cancelled", "Shape selection cancelled. Exiting.")
        sys.exit(0)

    data = read_input_file(input_file)
    image = generate_image(data, shape=shape_choice)
    image.save(output_file, "PNG")
    messagebox.showinfo("Completed", f"Image saved to {output_file}")

if __name__ == "__main__":
    main()
